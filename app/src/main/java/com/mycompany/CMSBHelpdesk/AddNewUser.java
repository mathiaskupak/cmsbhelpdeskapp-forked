package com.mycompany.CMSBHelpdesk;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;


public class AddNewUser extends ActionBarActivity implements View.OnClickListener {

    Button mcancel, addUser;
    EditText mNewName, mNewEmail, mNewTel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_user);

        initialise();

        backToMain(mcancel);
        backToMain(addUser);
    }

    public void initialise(){
        mcancel = (Button)findViewById(R.id.cancelBtn);
        addUser = (Button)findViewById(R.id.addNewUser);
        mNewName = (EditText) findViewById(R.id.newName);
        mNewEmail = (EditText) findViewById(R.id.newEmail);
        mNewTel = (EditText) findViewById(R.id.newTel);
    }

    public void backToMain(View button){
        final Context context = this;

        switch(button.getId()){
            case R.id.cancelBtn:
                mcancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getApplicationContext(), "Goodbye", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, AddCase.class);
                        startActivity(intent);
                        finish();
                    }
                });
            case R.id.addNewUser:
                addUser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addUser();
                        //Toast.makeText(getApplicationContext(), "New User Added!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, AddCase.class);
                        startActivity(intent);
                        finish();
                    }
                });
        }
    }

    public void addUser(){
        sharedPreference.setString(this, "newUse", mNewName.getText().toString());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        final Context context = this;

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(context, Settings.class);
            startActivity(intent);
            return true;
        }
        else if(id == R.id.backBtn){
            this.finish();
            return true;
        }
        if(id==R.id.mainMenu){
            Intent intent = new Intent(context, MainActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

    }

    //Closes the keyboard of you tap anywhere else on the screen
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

}
